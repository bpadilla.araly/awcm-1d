function L_out = reconstruction_check(Type, jlev, order, L_in)
%RECONSTRUCTION CHECK if all the supporting wavelets are present and does the correction if necessary.
% This must be done because maybe through the adding of neighbours some wavelets are placed without support.
%
% Type - 'internal' or 'periodic'
% jlev - number of jleves (clev can be calculated from that)
% L_in - mask of significan wavelet
% order - order of the wavelet 
%
% Written: Oleg V. Vasilyev, 4 October, 2018

ende       = length(L_in);
L_out       = L_in;
      
switch lower(Type)
case 'interval'
    %CALCULATION OF n - THE RESOLUTION ON THE FINEST LEVEL
    n_range = [-fix((order+1)/2):order-fix((order+1)/2)];

    %LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
    for j = jlev-1:-1:1
        %STRIDE
        s = 2^(jlev-j-1);
        
        %SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
        D_ind = [1+s:2*s:ende];
        D_ind = D_ind(find(L_out(D_ind)));
        
        %RECONSTRUCTION CHECK: IF ANY D IS FLAGGED THEN IT'S NEIGBOURS ON THE LEVEL, REQUIRED FOR PREDICT STAGE, MUST BE FLAGGED
        for i = D_ind
            n_range_corr=D_range_corr(i,ende,s,n_range);
            order_corr = length(n_range_corr) - 1;
            for k = 1:order_corr+1
                L_out(i+(2*n_range_corr(k)+1)*s) =  L_out(i+(2*n_range_corr(k)+1)*s) | L_out(i);
            end
        end
        
    end
    
case 'periodic'
    %CALCULATION OF n - THE RESOLUTION ON THE FINEST LEVEL

    n_range = [-fix((order+1)/2):order-fix((order+1)/2)];
    
    %LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
    for j = jlev-1:-1:1
        %STRIDE
        s = 2^(jlev-j-1);
        
        %SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
        D_ind = [1+s:2*s:ende];
        D_ind = D_ind(find(L_out(D_ind)));
        
        %REDCONSTRUCTION CHECK: IF ANY D IS FLAGGED THEN IT'S NEIGBOURS ON THE LEVEL, REQUIRED FOR PREDICT STAGE, MUST BE FLAGGED
        for k = 1:order+1
            L_out(prd_corr(D_ind+(2*n_range(k)+1)*s,ende)) = L_out(prd_corr(D_ind+(2*n_range(k)+1)*s,ende)) | L_out(D_ind);
        end
    end
        
 end