%THIS SCRIPT TESTS THE PERIODIC WAVELET TRANSFORM WFT
% Written: Oleg V. Vasilyev, 14 October, 2018

clear ALL;

Type        = 'periodic'
%SPECIFY RESOLUTION
cl          = 2^3;                          %RESOLUTION ON COARSEST LEVEL
jlev        = 3;                                  %NUMBER OF LEVELS
Jmx         = 5;                                  %MAXIMUM NUMBER OF LEVELS ALLOWED
jlev_ori    = jlev;
n_ori       = cl*2^(jlev-1);                %RESOLUTION ON FINEST LEVEL - THIS IS UNIQUE POINTS, THE PERIODIC ONE IS DROPPED
order       = 1;
acc         = 0.1;                          %ACCURACY CONTROL OVER WAVELET AMPLITUDES
Flag        = 'forward';
L_ori       = logical(ones(1,n_ori));
adjust_jlev = false;


%SPECIFY SIGNAL
%rand('state',0)                             %Reset random generator
F_ori       = detrend(cumsum(rand(n_ori,1)))';  %Generate random, but smooth enough signal

scl = max(abs(F_ori(L_ori)));

%FORWARD WAVELET TRANSFORM ==========================================================================================================
Wtf                         = wtf(Type, F_ori, jlev_ori, order, 'forward', L_ori);

L_sig                       = significant(Wtf, L_ori, scl, acc, jlev);

[F_ori, L_ori, jtmp, ende]  = adjust_lev(F_ori, L_sig, jlev, Jmx);
[Wtf,   L_sig, jlev, ende]  = adjust_lev(Wtf, L_sig, jlev, Jmx);

L = reconstruction_check(Type, jlev, order, L_sig);

%INVERSE WAVLET TRANSFORM ===========================================================================================================
Wtf_inv    = wtf(Type, Wtf, jlev, order, 'Inverse', L);

%INVERSE WAVLET TRANSFORM ===========================================================================================================
 Wtf(~L)        = 0.0;
 L_all          = logical(ones(size(L)));
 Wtf_inv_all    = wtf(Type, Wtf, jlev, order, 'Inverse', L_all);

%VISUALIZE SIGNAL ===================================================================================================================
X_vec       = 1:ende;              %X-grid on finest level
figure(111), clf; zoom on;
subplot(211);
plot(X_vec(L_ori), F_ori(L_ori), 'k-', X_vec(L), Wtf_inv(L), 'rx', X_vec(L_all), Wtf_inv_all(L_all), 'r-'); 
title(['Acc: ', num2str(acc), '  Sav: ', num2str(1-sum(L)/ende)]);
Achsen  = axis;
axis([0, ende, Achsen(3:4)]);
subplot(212)
hold on

%SETUP THE LEVELS
Level   = jlev*ones(size(Wtf_inv));
for j = jlev-1:-1:1
    s                   = 2^(jlev-j-1);
    Level(1:2*s:ende)   = j;
end

plot(X_vec, Level, 'kx');
plot(X_vec(L), Level(L), 'rx');
set(gca, 'XTick', [], 'YTick', []);
axis([0, ende+1, 0, jlev+1]);
box on;



