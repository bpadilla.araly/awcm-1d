function [x, dy, d2y] = diff_wlt(y)
%DIFF_WLT calciulates derivative using global definitions
%
% F         is the function 
%
% Written: Oleg V. Vasilyev, 4 October, 2018

%global L G j_df Type jlev order h der_order
global L Type jlev order h der_order XX Jmx

X      = XX(1:2^(Jmx-jlev):end);  
x  = reshape(X(L),size(y));
F(L) = y;
%[dF, d2F] = calc_derivative_mask(F, L, G, j_df, Type, jlev, order, h, der_order);[dF, d2F] = calc_derivative_mask(F, L, G, j_df, Type, jlev, order, h, der_order);
[dF, d2F] = calc_derivative(F, L, Type, jlev, order, h, der_order);
dy  = reshape(dF(L),size(y));
d2y = reshape(d2F(L),size(y));