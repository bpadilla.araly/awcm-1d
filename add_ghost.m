function [G, j_df] = add_ghost(L, Type, jlev, der_order)
% G         is the mask for points for derivative calculation
% j_df      is the array of levels at which the derivative is calculated
% L         is the mask where F is given,
% Type      is either internal or periodic
% jlev      is the number of jleves
% der_order is polynomial order of derivative approximation
%
% Written: Oleg V. Vasilyev, 14 October, 2018

ende       = length(L);

%Calculates derivative level
j_df = ones(size(L));
j_df(~L)=0;
n_range = [-1:0]; % just near neighbors to calculate derivatieve level
for j=1:jlev-1
    s = 2^(jlev-j-1);

    D_ind = [1+s:2*s:ende];
    D_ind = D_ind(find(L(D_ind)));
    j_df(D_ind)  = j+1;
end

switch lower(Type)
case 'interval'
    
    for j=1:jlev-1
        s = 2^(jlev-j-1);
        
        D_ind = [1+s:2*s:ende];
        C_ind = [1:2*s:ende];
        D_ind = D_ind(find(L(D_ind)));
        C_ind = C_ind(find(L(C_ind)));
        
        for i = D_ind
            n_range_corr=G_range_corr(i,ende,s,n_range);
            for k = 1:length(n_range_corr)
                j_df(i+(2*n_range_corr(k)+1)*s)=j+1;
            end
        end     
    end

case 'periodic'
    
    for j = 1:jlev-1                                                  
        s = 2^(jlev-j-1);
        
        D_ind = [1+s:2*s:ende];
        C_ind = [1:2*s:ende];
        D_ind = D_ind(find(L(D_ind)));
        C_ind = C_ind(find(L(C_ind)));
        
        for k = 1:length(n_range)
            j_df(prd_corr(D_ind+(2*n_range(k)+1)*s,ende))=j+1;
        end
    end
end

%Add ghost points for derivative calculation
G=L;
n_range = [-fix((der_order+1)/2):fix((der_order+1)/2)];
switch lower(Type)
case 'interval'
    
    for j=1:jlev
        s = 2^(jlev-j);
        
        G_ind = [1:s:ende];
        G_ind = G_ind(find(L(G_ind)));
        
        for i = G_ind(find(j == j_df(G_ind)))
            n_range_corr=G_range_corr(i,ende,s,n_range);
            for k = 1:length(n_range_corr)
                G(i+n_range_corr(k)*s)=true;
            end
        end     
    end

case 'periodic'
    
    for j = 1:jlev
        s = 2^(jlev-j);
        
        G_ind = [1:s:ende];
        G_ind = G_ind(find(L(G_ind)));
        G_ind = G_ind(find(j == j_df(G_ind)));
        for k = 1:length(n_range)
            G(prd_corr(G_ind+n_range(k)*s,ende))=true;
        end
    end
end

