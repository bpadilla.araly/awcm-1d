function Ind = prd_corr(Ind, n)
%PRD_CORR wraps the points around

Ind = rem(Ind-1+n, n)+1;

