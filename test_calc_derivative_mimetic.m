%THIS SCRIPT TESTS THE DERIVATIVES USING WAVELET TRANSFORM WFT ON AN INTERVAL
% Written: Oleg V. Vasilyev, 14 October, 2018

clear all;

Type        = 'periodic';                         % Domain type: 'interval' or 'periodic'
cl          = 2^3;                                %RESOLUTION ON COARSEST LEVEL
jlev        = 9;                                  %NUMBER OF LEVELS
Jmx         = 9;                                  %MAXIMUM NUMBER OF LEVELS ALLOWED
ende        = 1+cl*2^(jlev-1);                    %RESOLUTION ON FINEST LEVEL FOR NON-PERIODIC CASE
order       = 3;
der_order   = 3;
acc         = 0.001;                                %ACCURACY CONTROL OVER WAVELET AMPLITUDES

switch lower(Type)
    case 'periodic'
        ende    = ende - 1;
end
L           = logical(ones(1,ende));

%SPECIFY SIGNAL
a      = [1.0, 1.0];
b      = [0.25, 0.25];
c      = [1.e5,0.02];
%SPECIFY SIGNAL
h      = 1.0/(ende-mod(ende,2));          %h - mesh spacing on the finest level
X      = h*[0:ende-1];                    %X - grid on finest level
F      = zeros(size(X));
dFa    = zeros(size(X));
d2Fa   = zeros(size(X));
for k = 1:length(a)
    F    = F    + a(k)*exp(-(X-b(k)).^2/c(k)^2);
    dFa  = dFa  - a(k)*exp(-(X-b(k)).^2/c(k)^2).*(2.0*(X-b(k))/c(k)^2);
    d2Fa = d2Fa + a(k)*exp(-(X-b(k)).^2/c(k)^2).*(4.0*(X-b(k)).^2/c(k)^4 - 2.0/c(k)^2);
%     F    = F    +         a(k)*sin(2.*pi*(X-b(k)));
%     dFa  = dFa  + 2.*pi  *a(k)*cos(2.*pi*(X-b(k)));
%     d2Fa = d2Fa - 4.*pi^2*a(k)*sin(2.*pi*(X-b(k)));
end
%  p =5;
%  F    =   X.^p;
%  dFa  = p*X.^(p-1);
%  F   = sin(2*pi*X);
%  dFa = 2*pi*cos(2*pi*X);
%ADAPT GRID================================
scl    = max(F(L))-min(F(L));

Fn    = wtf(Type, F,  jlev, order, 'forward', L);

L_sig = significant(Fn, L, scl, acc, jlev);

[Fn, L_sig, jlev, ende] = adjust_lev(Fn, L_sig, jlev, Jmx);

L_sig = add_adjacent(Type, L_sig, jlev);

L = reconstruction_check(Type, jlev, order, L_sig);

[L, j_df] = add_deriv(L, Type, jlev, order);

L = reconstruction_check(Type, jlev, order, L);


% [G, j_df] = add_ghost(L, Type, jlev, order);
%=========================================

%SPECIFY SIGNAL on the new mesh
h      = 1.0/(ende-mod(ende,2));          %h - mesh spacing on the finest level
X      = h*[0:ende-1];                    %X - grid on finest level
F      = zeros(size(X));
dFa    = zeros(size(X));
d2Fa   = zeros(size(X));
for k = 1:length(a)
    F    = F    + a(k)*exp(-(X-b(k)).^2/c(k)^2);
    dFa  = dFa  - a(k)*exp(-(X-b(k)).^2/c(k)^2).*(2.0*(X-b(k))/c(k)^2);
    d2Fa = d2Fa + a(k)*exp(-(X-b(k)).^2/c(k)^2).*(4.0*(X-b(k)).^2/c(k)^4 - 2.0/c(k)^2);
%     F    = F    +         a(k)*sin(2.*pi*(X-b(k)));
%     dFa  = dFa  + 2.*pi  *a(k)*cos(2.*pi*(X-b(k)));
%     d2Fa = d2Fa - 4.*pi^2*a(k)*sin(2.*pi*(X-b(k)));
end
%  F    =   X.^p;
%  dFa  = p*X.^(p-1);
%  F   = sin(2*pi*X);
%  dFa = 2*pi*cos(2*pi*X);
%==============================

Fn    = wtf(Type, Fn, jlev, order, 'inverse', L);
Fn(L) = F(L);
Fn(~L) = NaN;

%[dF, d2F] = calc_derivative(Fn, L, Type, jlev, order, h, der_order);

dF = calc_derivative_mimetic(Fn, L, Type, jlev, order, h);

clf;
figure(111);
subplot(311);
%plot(X, dFa, 'k-');
%hold on
plot(X(L), dF(L), 'rx');
%plot(X, d2Fa/max(abs(d2Fa)), 'k-');
%plot(X(L), d2F(L)/max(abs(d2Fa)),'bx');
title(['Normalized Derirvative:  Compression: ', num2str(1-sum(L)/ende)]);
Achsen  = axis;
axis([0, 1, Achsen(3:4)]);

%VISUALIZE TREE ===================================================================================================================
subplot(312)
Level   = jlev*ones(size(dF));
for j = jlev-1:-1:1
    s = 2^(jlev-j-1);
    Level(1:2*s:ende)   = j;
end
plot(X,    Level,    'kx');
hold on
%plot(X(G), Level(G), 'bo');
plot(X(L), Level(L), 'rx');

set(gca, 'YTick', [1:jlev]);
axis([0, 1, 0, jlev+1]);
title(['Original jlev: ', num2str(jlev), '  New jlev: ', num2str(jlev)]);

%WAIT FOR PROMPT
subplot(313);
%semilogy(X(L), abs(dFa(L)-dF(L))/max(abs(dFa)),'-r');
plot(X(L), dFa(L)-dF(L),'-r');
%plot(X(L), dF(L),'-r');
title(['Absolute Derivative Error:' sprintf(' err_{D}=%f', max(abs(dFa(L)-dF(L))))]);
%title(['Normalized Derivative Error' sprintf(' Error_{D}=%f, Error_{D2}=%f', max(abs(dFa(L)-dF(L))),max(abs(d2Fa(L)-d2F(L))))]);
%hold on
%semilogy(X(L), abs(d2Fa(L)-d2F(L))/max(abs(d2Fa)),'-b');



