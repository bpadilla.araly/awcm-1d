# AWCM-1D

Matlab 1-D Demo for Adaptive Wavelet Collocation Method (AWCM) (Vasilyev & Bowman, 2000)


All demo files are written for clarity of understanding of the algorithm without any consideration for efficiency.

References:

Vasilyev, O.V. and Bowman, C., “Second Generation Wavelet Collocation Method for the Solution of Partial Differential Equations,” Journal of Computational Physics, 165, pp. 630–693, 2000.
